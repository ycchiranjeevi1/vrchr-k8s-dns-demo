# DNS Wildcard Demo

* Create cluster using Terraform

```
$ cd Terraform
$ terraform init
$ terraform plan -out plan.out
$ terraform apply plan.out
```

This will:

1) Create a Scaleway cluster
1) Add Nginx Ingress chart
1) Add Cert manager chart

* Get Scaleway cert manager webhook
```
$ git clone https://github.com/scaleway/cert-manager-webhook-scaleway.git
$ cd cert-manager-webhook-scaleway
$ helm install scaleway-webhook deploy/scaleway-webhook --set secret.accessKey=changeme --set secret.secretKey=changeme --set certManager.serviceAccountName=jetstack-cert-manager --namespace=cert-manager
```

* Apply issuer and cert config

```
$ kubectl apply -f k8s/scaleway-issuer.yaml
$ kubectl apply -f k8s/scaleway-cert.yaml
```

Now you can deploy your applications using the wilcard certificate

```
$ kubectl apply -f k8s/dep_green.yaml
$ kubectl apply -f k8s/dep_green.yaml
$ kubectl apply -f k8s/dep_green.yaml
```
Use the wilcard cert:

```
$ kubectl apply -f k8s/ingress.yaml
```


## Ressources

* https://github.com/scaleway/terraform-provider-scaleway/blob/master/docs/resources/domain_record.md
* https://particule.io/blog/scaleway-cert-manager/

## Notes

* Be careful using the correct serviceaccount for cert manager, which is jetstack-cert-manager using helm
* DNS propagation can be long
* Edit labels

```
$ kubectl label namespace cert-manager certmanager.k8s.io/disable-validation=true
```