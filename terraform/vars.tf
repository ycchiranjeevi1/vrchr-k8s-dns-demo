variable "k8s_cluster_name" {
  description = "Name of the k8s cluster"
  type = string
  default = "k8s-sca-wild"
}

variable "k8s_pool_name" {
  description = "value"
  type = string
  default = "k8s-sca-wild-pool"
}