resource "scaleway_k8s_cluster" "k8s_cluster" {
  name = var.k8s_cluster_name
  version = "1.22.2"
  cni = "cilium"
}

resource "scaleway_k8s_pool" "k8s_pool" {
  cluster_id = scaleway_k8s_cluster.k8s_cluster.id
  name = var.k8s_pool_name
  node_type = "DEV1-M"
  size = 2
  autohealing = true
}

resource "local_file" "kubeconfig" {
  content = scaleway_k8s_cluster.k8s_cluster.kubeconfig[0].config_file
  filename = "${path.module}/${scaleway_k8s_cluster.k8s_cluster.name}-kubeconfig"
  file_permission = "0600"
}

resource "null_resource" "kubeconfig" {
  depends_on = [scaleway_k8s_pool.k8s_pool] # at least one pool here
  triggers = {
    host                   = scaleway_k8s_cluster.k8s_cluster.kubeconfig[0].host
    token                  = scaleway_k8s_cluster.k8s_cluster.kubeconfig[0].token
    cluster_ca_certificate = scaleway_k8s_cluster.k8s_cluster.kubeconfig[0].cluster_ca_certificate
  }
}